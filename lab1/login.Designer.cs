﻿namespace lab1
{
    partial class login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.l_login = new System.Windows.Forms.Label();
            this.l_pass = new System.Windows.Forms.Label();
            this.tb_login = new System.Windows.Forms.TextBox();
            this.tb_pass = new System.Windows.Forms.TextBox();
            this.b_enter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // l_login
            // 
            this.l_login.AutoSize = true;
            this.l_login.Location = new System.Drawing.Point(25, 20);
            this.l_login.Name = "l_login";
            this.l_login.Size = new System.Drawing.Size(38, 13);
            this.l_login.TabIndex = 0;
            this.l_login.Text = "Логин";
            // 
            // l_pass
            // 
            this.l_pass.AutoSize = true;
            this.l_pass.Location = new System.Drawing.Point(25, 59);
            this.l_pass.Name = "l_pass";
            this.l_pass.Size = new System.Drawing.Size(43, 13);
            this.l_pass.TabIndex = 1;
            this.l_pass.Text = "пароль";
            // 
            // tb_login
            // 
            this.tb_login.Location = new System.Drawing.Point(28, 36);
            this.tb_login.Name = "tb_login";
            this.tb_login.Size = new System.Drawing.Size(135, 20);
            this.tb_login.TabIndex = 2;
            this.tb_login.Text = "ADMIN";
            // 
            // tb_pass
            // 
            this.tb_pass.Location = new System.Drawing.Point(28, 75);
            this.tb_pass.Name = "tb_pass";
            this.tb_pass.PasswordChar = '*';
            this.tb_pass.Size = new System.Drawing.Size(135, 20);
            this.tb_pass.TabIndex = 3;
            // 
            // b_enter
            // 
            this.b_enter.Location = new System.Drawing.Point(28, 114);
            this.b_enter.Name = "b_enter";
            this.b_enter.Size = new System.Drawing.Size(135, 23);
            this.b_enter.TabIndex = 4;
            this.b_enter.Text = "Войти";
            this.b_enter.UseVisualStyleBackColor = true;
            this.b_enter.Click += new System.EventHandler(this.b_enter_Click);
            // 
            // login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(202, 171);
            this.Controls.Add(this.b_enter);
            this.Controls.Add(this.tb_pass);
            this.Controls.Add(this.tb_login);
            this.Controls.Add(this.l_pass);
            this.Controls.Add(this.l_login);
            this.Name = "login";
            this.Text = "Войти";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label l_login;
        private System.Windows.Forms.Label l_pass;
        private System.Windows.Forms.TextBox tb_login;
        private System.Windows.Forms.TextBox tb_pass;
        private System.Windows.Forms.Button b_enter;
    }
}