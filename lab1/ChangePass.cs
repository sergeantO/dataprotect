﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class ChangePass : Form
    {
        dataprovider _dp;

        public ChangePass(dataprovider dp)
        {
            InitializeComponent();
            _dp = dp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mes = _dp.ChangePass(textBox1.Text);
            MessageBox.Show(mes);
            Close();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == textBox2.Text)
            {
                button1.Enabled = true;
            }
        }
    }
}
