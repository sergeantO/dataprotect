﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class AddUser : Form
    {
        dataprovider _dp;

        public AddUser(dataprovider dp)
        {
            InitializeComponent();
            _dp = dp;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _dp.Add(textBox1.Text);
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
