﻿namespace lab1
{
    partial class mainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.b_changePass = new System.Windows.Forms.Button();
            this.b_addUser = new System.Windows.Forms.Button();
            this.lBox_users = new System.Windows.Forms.ListBox();
            this.chbox_block = new System.Windows.Forms.CheckBox();
            this.chbox_bounded = new System.Windows.Forms.CheckBox();
            this.b_exit = new System.Windows.Forms.Button();
            this.dataproviderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataproviderBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // b_changePass
            // 
            this.b_changePass.Location = new System.Drawing.Point(270, 12);
            this.b_changePass.Name = "b_changePass";
            this.b_changePass.Size = new System.Drawing.Size(163, 23);
            this.b_changePass.TabIndex = 0;
            this.b_changePass.Text = "Сменить пароль";
            this.b_changePass.UseVisualStyleBackColor = true;
            // 
            // b_addUser
            // 
            this.b_addUser.Location = new System.Drawing.Point(270, 51);
            this.b_addUser.Name = "b_addUser";
            this.b_addUser.Size = new System.Drawing.Size(163, 23);
            this.b_addUser.TabIndex = 1;
            this.b_addUser.Text = "Добавить пользователя";
            this.b_addUser.UseVisualStyleBackColor = true;
            // 
            // lBox_users
            // 
            this.lBox_users.FormattingEnabled = true;
            this.lBox_users.Location = new System.Drawing.Point(12, 12);
            this.lBox_users.Name = "lBox_users";
            this.lBox_users.Size = new System.Drawing.Size(230, 186);
            this.lBox_users.TabIndex = 2;
            // 
            // chbox_block
            // 
            this.chbox_block.AutoSize = true;
            this.chbox_block.Location = new System.Drawing.Point(270, 95);
            this.chbox_block.Name = "chbox_block";
            this.chbox_block.Size = new System.Drawing.Size(104, 17);
            this.chbox_block.TabIndex = 3;
            this.chbox_block.Text = "Заблокировать";
            this.chbox_block.UseVisualStyleBackColor = true;
            // 
            // chbox_bounded
            // 
            this.chbox_bounded.AutoSize = true;
            this.chbox_bounded.Location = new System.Drawing.Point(270, 130);
            this.chbox_bounded.Name = "chbox_bounded";
            this.chbox_bounded.Size = new System.Drawing.Size(207, 17);
            this.chbox_bounded.TabIndex = 4;
            this.chbox_bounded.Text = "Установить ограничения на пароль";
            this.chbox_bounded.UseVisualStyleBackColor = true;
            // 
            // b_exit
            // 
            this.b_exit.Location = new System.Drawing.Point(270, 175);
            this.b_exit.Name = "b_exit";
            this.b_exit.Size = new System.Drawing.Size(163, 23);
            this.b_exit.TabIndex = 5;
            this.b_exit.Text = "Выход";
            this.b_exit.UseVisualStyleBackColor = true;
            this.b_exit.Click += new System.EventHandler(this.b_exit_Click);
            // 
            // dataproviderBindingSource
            // 
            this.dataproviderBindingSource.DataSource = typeof(lab1.dataprovider);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 221);
            this.Controls.Add(this.b_exit);
            this.Controls.Add(this.chbox_bounded);
            this.Controls.Add(this.chbox_block);
            this.Controls.Add(this.lBox_users);
            this.Controls.Add(this.b_addUser);
            this.Controls.Add(this.b_changePass);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataproviderBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_changePass;
        private System.Windows.Forms.Button b_addUser;
        private System.Windows.Forms.ListBox lBox_users;
        private System.Windows.Forms.CheckBox chbox_block;
        private System.Windows.Forms.CheckBox chbox_bounded;
        private System.Windows.Forms.Button b_exit;
        private System.Windows.Forms.BindingSource dataproviderBindingSource;
    }
}

