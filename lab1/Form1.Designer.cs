﻿namespace lab1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.b_changePass = new System.Windows.Forms.Button();
            this.b_addUser = new System.Windows.Forms.Button();
            this.lBox_users = new System.Windows.Forms.ListBox();
            this.chbox_block = new System.Windows.Forms.CheckBox();
            this.chbox_bounded = new System.Windows.Forms.CheckBox();
            this.b_exit = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.dataproviderBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataproviderBindingSource)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // b_changePass
            // 
            this.b_changePass.Location = new System.Drawing.Point(248, 39);
            this.b_changePass.Name = "b_changePass";
            this.b_changePass.Size = new System.Drawing.Size(176, 23);
            this.b_changePass.TabIndex = 0;
            this.b_changePass.Text = "Сменить свой пароль";
            this.b_changePass.UseVisualStyleBackColor = true;
            this.b_changePass.Click += new System.EventHandler(this.b_changePass_Click);
            // 
            // b_addUser
            // 
            this.b_addUser.Location = new System.Drawing.Point(12, 39);
            this.b_addUser.Name = "b_addUser";
            this.b_addUser.Size = new System.Drawing.Size(173, 23);
            this.b_addUser.TabIndex = 1;
            this.b_addUser.Text = "Добавить пользователя";
            this.b_addUser.UseVisualStyleBackColor = true;
            this.b_addUser.Click += new System.EventHandler(this.b_addUser_Click);
            // 
            // lBox_users
            // 
            this.lBox_users.FormattingEnabled = true;
            this.lBox_users.Location = new System.Drawing.Point(12, 78);
            this.lBox_users.Name = "lBox_users";
            this.lBox_users.Size = new System.Drawing.Size(230, 147);
            this.lBox_users.TabIndex = 2;
            this.lBox_users.SelectedIndexChanged += new System.EventHandler(this.lBox_users_SelectedIndexChanged);
            // 
            // chbox_block
            // 
            this.chbox_block.AutoSize = true;
            this.chbox_block.Location = new System.Drawing.Point(248, 96);
            this.chbox_block.Name = "chbox_block";
            this.chbox_block.Size = new System.Drawing.Size(104, 17);
            this.chbox_block.TabIndex = 3;
            this.chbox_block.Text = "Заблокировать";
            this.chbox_block.UseVisualStyleBackColor = true;
            this.chbox_block.CheckedChanged += new System.EventHandler(this.chbox_block_CheckedChanged);
            // 
            // chbox_bounded
            // 
            this.chbox_bounded.AutoSize = true;
            this.chbox_bounded.Location = new System.Drawing.Point(248, 131);
            this.chbox_bounded.Name = "chbox_bounded";
            this.chbox_bounded.Size = new System.Drawing.Size(207, 17);
            this.chbox_bounded.TabIndex = 4;
            this.chbox_bounded.Text = "Установить ограничения на пароль";
            this.chbox_bounded.UseVisualStyleBackColor = true;
            this.chbox_bounded.CheckedChanged += new System.EventHandler(this.chbox_bounded_CheckedChanged);
            // 
            // b_exit
            // 
            this.b_exit.Location = new System.Drawing.Point(270, 202);
            this.b_exit.Name = "b_exit";
            this.b_exit.Size = new System.Drawing.Size(163, 23);
            this.b_exit.TabIndex = 5;
            this.b_exit.Text = "Выход";
            this.b_exit.UseVisualStyleBackColor = true;
            this.b_exit.Click += new System.EventHandler(this.b_exit_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(270, 173);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "Сменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataproviderBindingSource
            // 
            this.dataproviderBindingSource.DataSource = typeof(lab1.dataprovider);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(462, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(94, 20);
            this.toolStripMenuItem1.Text = "О программе";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 242);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.b_exit);
            this.Controls.Add(this.chbox_bounded);
            this.Controls.Add(this.chbox_block);
            this.Controls.Add(this.lBox_users);
            this.Controls.Add(this.b_addUser);
            this.Controls.Add(this.b_changePass);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataproviderBindingSource)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_changePass;
        private System.Windows.Forms.Button b_addUser;
        private System.Windows.Forms.ListBox lBox_users;
        private System.Windows.Forms.CheckBox chbox_block;
        private System.Windows.Forms.CheckBox chbox_bounded;
        private System.Windows.Forms.Button b_exit;
        private System.Windows.Forms.BindingSource dataproviderBindingSource;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

