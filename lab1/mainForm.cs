﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class mainForm : Form
    {
        dataprovider dp = new dataprovider("data");

        public mainForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            login loginForm = new login(dp);
            bool sucses = false;
            string message = "";


            loginForm.ShowDialog();

            if (!sucses)
                Close();

            lBox_users.DataSource = dp.Data();
        }

        private void b_exit_Click(object sender, EventArgs e)
        {
            dp.Save();
            Close();
        }
    }
}
