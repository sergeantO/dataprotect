﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace lab1
{
    public enum Mode
    {
        admin,
        user,
        notFound,
        isBlocked,
        wrongPass
    }

    [Serializable]
    public class User
    {
        public string login { get; set; }
        public string password;
        public int    passLenght; 
        public bool   blocked;
        public bool   bounded;
    }

    [Serializable]
    public class dataprovider
    {
        List<User> users = new List<User>();
        User user;
        string _file;
        XmlSerializer formatter = new XmlSerializer(typeof(List<User>));

        public dataprovider(string file)
        {
            _file = file;
            // десериализация
            using (FileStream fs = new FileStream($"{_file}.xml", FileMode.OpenOrCreate))
            {
                try
                {
                    users = (List<User>)formatter.Deserialize(fs);
                }
                catch
                {
                    users = new List<User>();
                    Add("ADMIN");
                }
                 
            }
        }

        public void Save()
        {
            // сериализация
            using (FileStream fs = new FileStream($"{_file}.xml", FileMode.Create))
            {
                formatter.Serialize(fs, users);
            }
        }

        public User Find (string name)
        {
            foreach (User u in users)
            {
                if (u.login == name)
                {
                    return u;
                }
            }
            return null;
        }

        public List<User> Data()
        {
            return users;
        }

        public void Add(string name)
        {
            if (Find(name) != null)
            {
                throw new Exception($"Имя <<{name}>> уже занято");
            }

            User newUser = new User
            {
                login = name,
                password = "",
                passLenght = 0,
                blocked = false,
                bounded = false,
            };

            users.Add(newUser);
        }

        public Mode Login(string name, string _password)
        {
            User tmp = Find(name);
            if (tmp != null)
            {
                user = tmp;
                if (user.password == _password)  
                {
                    if (user.blocked)
                        return Mode.isBlocked;

                    if (user.login == "ADMIN")
                        return Mode.admin;
                    
                    return Mode.user;
                }
                return Mode.wrongPass;
            }
            return Mode.notFound;
        }

        public string ChangePass(string newPass)
        {
            if (user.bounded)
            {
                if (!PassBound(newPass))
                { 
                    return "Пароль должен состоять только из латинских букв и символов кириллицы";
                }
            }

            user.password = newPass;
            user.passLenght = newPass.Length;
            return "Пароль успешно изменен";

        }

        private bool PassBound(string pass)
        {
            foreach(char ch in pass)
            {
                if (!Char.IsLetter(ch))
                    return false;
            }
            return true;
        }

        public void BlockUser(bool block)
        {
            user.blocked = block;
        }

        public void BlockUser(User u, bool block)
        {
            u.blocked = block;
        }

        public void BoundUser(bool bound)
        {
            user.bounded = bound;
        }

        public void BoundUser(User u, bool bound)
        {
            u.bounded = bound;
        }
    }
}
