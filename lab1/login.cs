﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    public partial class login : Form
    {
        public Mode Result { get; private set; } = Mode.notFound;
        dataprovider _dp;
        int error = 3;

        public login(dataprovider dp)
        {
            InitializeComponent();
            _dp = dp;
        }

        private void b_enter_Click(object sender, EventArgs e)
        {
            Result = _dp.Login(tb_login.Text, tb_pass.Text);
            switch (Result)
            {
                case Mode.isBlocked:
                    MessageBox.Show("Пользователь заблокирован");
                    break;
                case Mode.notFound:
                    MessageBox.Show("Пользователь не найден");
                    break;
                case Mode.wrongPass:
                    error--;
                    MessageBox.Show($"Неверный пароль. Осталось {error} попыток");
                    if (error < 1)
                        Close();
                    break;
                case Mode.user:
                    MessageBox.Show("Вы вошли как пользователь");
                    Close();
                    break;
                case Mode.admin:
                    MessageBox.Show("Вы вошли как администратор");
                    Close();
                    break;
            }
        }
    }
}
