﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab1
{
    
     
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        dataprovider dp = new dataprovider("data");

        private void Form1_Load(object sender, EventArgs e)
        {
            login loginForm = new login(dp);
            loginForm.ShowDialog();
            switch (loginForm.Result)
            {
                case Mode.isBlocked:
                case Mode.notFound:
                case Mode.wrongPass:
                default:
                    Close();
                    break;
                case Mode.user:
                    b_addUser.Enabled = false;
                    chbox_bounded.Enabled = false;
                    chbox_block.Enabled = false;
                    lBox_users.Enabled = false;
                    break;
                case Mode.admin:
                    RefreshListBox();
                    break;
            }
        }

        private void b_exit_Click(object sender, EventArgs e)
        {
            dp.Save();
            Close();
        }

        private void b_addUser_Click(object sender, EventArgs e)
        {
            AddUser au = new AddUser(dp);
            au.ShowDialog();
            RefreshListBox();
        }

        private void RefreshListBox()
        {
            lBox_users.DataSource = null;
            lBox_users.DataSource = dp.Data();
            lBox_users.DisplayMember = "login";
        }

        private void b_changePass_Click(object sender, EventArgs e)
        {
            ChangePass cp = new ChangePass(dp);
            cp.ShowDialog();
        }

        private void chbox_block_CheckedChanged(object sender, EventArgs e)
        {
            dp.BlockUser(tmp, chbox_block.Checked);
        }

        private void chbox_bounded_CheckedChanged(object sender, EventArgs e)
        {
            dp.BoundUser(tmp, chbox_bounded.Checked);
        }

        User tmp;
        private void lBox_users_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lBox_users.SelectedItem != null)
            {
                tmp = (User)lBox_users.SelectedItem;
                chbox_block.Checked = tmp.blocked;
                chbox_bounded.Checked = tmp.bounded;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1_Load(null, null);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }
    }
}
